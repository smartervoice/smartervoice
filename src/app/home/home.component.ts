import { Component, OnInit } from '@angular/core';
import {AuthenticateService} from "../login/loginService/authenticate.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [AuthenticateService],
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    addPurchaseDiv : boolean;
    editPurchaseDiv : boolean;
    purchaseDiv : boolean;
    homeDiv : boolean;
    addCampaignDiv : boolean;
    modifyCampaignDiv : boolean;
    dailyRecordDiv : boolean;
    weeklyRecordDiv : boolean;
    campaignRecordDiv : boolean;

  constructor(private _service:AuthenticateService) {
        this.addPurchaseDiv = false; 
        this.editPurchaseDiv = false;
        this.purchaseDiv = false;
        this.homeDiv = true;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
   }

  ngOnInit() {
    this._service.checkCredentials();

  }
    

  logout():void {
    this._service.logout();
  }
 

    addPurchase(){
        this.addPurchaseDiv = true;
        this.editPurchaseDiv = false;
        this.purchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }
    editPurchase(){
        this.addPurchaseDiv = false;
        this.editPurchaseDiv = true;
        this.purchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }

    homePage(){
        this.addPurchaseDiv = false;
        this.editPurchaseDiv = false;
        this.purchaseDiv = false;
        this.homeDiv = true;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }

    addCampaign(){
        this.addPurchaseDiv = false;
        this.editPurchaseDiv = false;
        this.purchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = true;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }


    modfyCampaign(){
        this.addPurchaseDiv = false;
        this.purchaseDiv = false;
        this.editPurchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = true;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }

    dailyReport(){
        this.addPurchaseDiv = false;
        this.purchaseDiv = false;
        this.editPurchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = true;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }

    weeklyReport(){
        this.addPurchaseDiv = false;
        this.purchaseDiv = false;
        this.editPurchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = true;
        this.campaignRecordDiv = false;
    }

    campReport(){
        this.addPurchaseDiv = false;
        this.purchaseDiv = false;
        this.editPurchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = true;
    }
    purchaseItem(){
        this.addPurchaseDiv = false;
        this.purchaseDiv = true;
        this.editPurchaseDiv = false;
        this.homeDiv = false;
        this.addCampaignDiv = false;
        this.modifyCampaignDiv = false;
        this.dailyRecordDiv = false;
        this.weeklyRecordDiv = false;
        this.campaignRecordDiv = false;
    }

    addPurchaseBtn(){

          alert('Form submitted.');

    }

    settings = {
          columns: {
            campaignName: {
                title: 'Campaign Name', filter: false 
            },
            startDate: {
                title: 'Start Date', filter: false
            },
            endDate: {
                title: 'End Date', filter: false
            },
            orderPerWeek: {
                title: 'Order Per week', filter: false
            },
            orderPerDay: {
                title: 'Max Order Per Day', filter: false
            },
            saleHours: {
                title: 'Sale Hours', filter: false
            }
          },
         actions: {
            add:false,
            position: 'right'
         } 
    };
    settings1 = {
          columns: {
            pName: {
                title: 'Name', filter: false 
            },
            productId: {
                title: 'Product Id', filter: false
            },
            emailAdd: {
                title: 'Email Address', filter: false
            },
            phoneNo: {
                title: 'Phone No', filter: false
            },
            statusId: {
                title: 'Status', filter: false
            }
          },
         actions: {
            add:false,
            edit:false,
            delete:false,
            position: 'right',
            custom: [
                {
                  name: 'Purchase',
                  title: 'Purchase ',
                }
            ]
        } 
    };
    settings2 = {
          columns: {
            pName: {
                title: 'Name', filter: false 
            },
            productId: {
                title: 'Product Id', filter: false
            },
            emailAdd: {
                title: 'Email Address', filter: false
            },
            phoneNo: {
                title: 'Phone No', filter: false
            },
            statusId: {
                title: 'Status', filter: false
            }
          },
         actions: {
            add:false,
            position: 'right'
        } 
    };

    onCustom(event) {
        alert(`Custom event '${event.action}' fired on row №: ${event.data.productId}`)
    }

    data1 = [
  {
    pName: "Abcd",
    productId: "11",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Pending"
  },
  {
    pName: "Abcd",
    productId: "12",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Purchased"
  },
  {
    pName: "Abcd",
    productId: "13",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Pending"
  },
  {
    pName: "Abcd",
    productId: "14",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Purchased"
  },
  {
    pName: "Abcd",
    productId: "15",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Purchased"
  },
  {
    pName: "Abcd",
    productId: "16",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Pending"
  },
  {
    pName: "Abcd",
    productId: "17",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Purchased"
  },
  {
    pName: "Abcd",
    productId: "18",
    emailAdd: "xyzgmail.com",
    phoneNo: "12478541652",
    statusId: "Purchased"
  }
];
data = [
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  },
  {
    campaignName: "Campaign A",
    startDate: "02/12/2018",
    endDate: "05/11/2018",
    orderPerWeek: "100",
    orderPerDay: "10",
    saleHours: "5"
  }
];
    
}


